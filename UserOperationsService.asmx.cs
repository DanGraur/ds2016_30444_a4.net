﻿using ClientOperationsWeb.Accessors.PackageAccess;
using ClientOperationsWeb.Accessors.RouteAccess;
using ClientOperationsWeb.Accessors.UserAccess;
using ClientOperationsWeb.Data.Domain;
using ClientOperationsWeb.SessionManager;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;

namespace ClientOperationsWeb
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class UserOperationsService : System.Web.Services.WebService
    {

        [WebMethod]
        public User GetUser(string username, string password)
        {
            UserDAO userDAO = new UserDAOAccessor(SessionManagerProvider.GetInstance());

            return userDAO.GetUser(username, password);
        }

        [WebMethod]
        public bool CheckIfUserExists(string username)
        {
            UserDAO userDAO = new UserDAOAccessor(SessionManagerProvider.GetInstance());

            return userDAO.CheckIfUserExists(username);
        }

        [WebMethod]
        public int AddUser(User user)
        {
            UserDAO userDAO = new UserDAOAccessor(SessionManagerProvider.GetInstance());

            return userDAO.AddUser(user);
        }

        [WebMethod]
        public Package[] GetUserPackages(int id)
        {
            PackageDAO packageDAO = new PackageDAOAccessor(SessionManagerProvider.GetInstance());

            return packageDAO.GetAllUserPackages(id).ToArray<Package>();
        }

        [WebMethod]
        public Package[] SearchUserPackages(int id, string name)
        {
            PackageDAO packageDAO = new PackageDAOAccessor(SessionManagerProvider.GetInstance());

            return packageDAO.SearchUserPackages(id, name).ToArray<Package>();
        }

        [WebMethod]
        public Route[] GetPackageRoute(int id)
        {
            RouteDAO routeDAO = new RouteDAOAccessor(SessionManagerProvider.GetInstance());

            return routeDAO
                .GetPackageRoute(id)
                .ToList<Route>()
                .OrderBy(x => x.time)
                .ToArray<Route>();
        }
    }
}
