﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientOperationsWeb.Data.Domain
{
    public class User
    {

        public User()
        {

        }

        public User(string username, string password, string type)
        {
            this.username = username;
            this.password = password;
            this.type = type;
        }


        public virtual int id { get; set; }
        public virtual string username { get; set; }
        public virtual string password { get; set; }
        public virtual string type { get; set; }
    }
}
