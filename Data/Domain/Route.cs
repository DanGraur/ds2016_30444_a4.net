﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientOperationsWeb.Data.Domain
{
    public class Route
    {
        public Route()
        {

        }

        public Route(int pid, int cid, long time)
        { 
            this.pid = pid;
            this.cid = cid;
            this.time = time;
        }

        public virtual int id { get; set; }
        public virtual int pid { get; set; }
        public virtual int cid { get; set; }
        public virtual long time { get; set; }
    }
}
