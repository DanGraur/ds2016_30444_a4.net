﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientOperationsWeb.Data.Domain
{
    public class Package
    {
        public Package()
        {

        }

        public Package(int suid, int duid, string name, string description, int scid, int dcid, int track)
        {
            this.suid = suid;
            this.duid = duid;
            this.name = name;
            this.description = description;
            this.scid = scid;
            this.dcid = dcid;
            this.track = track;
        }

        public virtual int id { get; set; }
        public virtual int suid { get; set; }
        public virtual int duid { get; set; }
        public virtual string name { get; set; }
        public virtual string description { get; set; }
        public virtual int scid { get; set; }
        public virtual int dcid { get; set; }
        public virtual int track { get; set; }
    }
}
