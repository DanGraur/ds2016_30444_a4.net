﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientOperationsWeb.SessionManager
{
    public class SessionManagerProvider
    {
        private static ISessionFactory sessionFactory;

        private SessionManagerProvider()
        {

        }

        public static ISessionFactory GetInstance()
        {
            if (sessionFactory == null)
                sessionFactory = new Configuration().Configure().BuildSessionFactory();

            return sessionFactory;
        }
    }
}
