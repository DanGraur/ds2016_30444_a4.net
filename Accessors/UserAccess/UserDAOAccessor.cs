﻿using ClientOperationsWeb.Data.Domain;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientOperationsWeb.Accessors.UserAccess
{

    public interface UserDAO
    {
        User GetUser(string username, string password);

        int AddUser(User user);

        bool CheckIfUserExists(string username);
    }

    public class UserDAOAccessor : UserDAO
    {
        private ISessionFactory factory;

        public UserDAOAccessor(ISessionFactory factory)
        {
            this.factory = factory;
        }

        public User GetUser(string username, string password)
        {
            ISession session = factory.OpenSession();
            ITransaction tx = null;
            User user = null;

            try
            {
                tx = session.BeginTransaction();

                IQuery searchQuery = session.CreateQuery("FROM User WHERE username = :username AND password = :password");
                searchQuery.SetParameter("username", username);
                searchQuery.SetParameter("password", password);

                user = (User) searchQuery.UniqueResult();

                tx.Commit();
            } 
            catch (HibernateException e)
            {
                if (tx != null)
                    tx.Rollback();
            }
            finally
            {
                session.Close();
            }

            return user;
        }

        public int AddUser(User user)
        {
            ISession session = factory.OpenSession();
            ITransaction tx = null;
            int id = 0;

            try
            {
                tx = session.BeginTransaction();

                id = (int) session.Save(user);

                tx.Commit();
            }
            catch (HibernateException e)
            {
                if (tx != null)
                    tx.Rollback();
            }
            finally
            {
                session.Close();
            }

            return id;
        }

        public bool CheckIfUserExists(string username)
        {
            ISession session = factory.OpenSession();
            ITransaction tx = null;
            bool exists = false;

            try
            {
                tx = session.BeginTransaction();

                if (session
                    .CreateQuery("FROM User WHERE lower(username) = :username")
                    .SetParameter("username", username.ToLower())
                    .UniqueResult() == null)
                    exists = false;
                else
                    exists = true;

                tx.Commit();
            }
            catch (HibernateException e)
            {
                if (tx != null)
                    tx.Rollback();
            }
            finally
            {
                session.Close();
            }

            return exists;
        }
    }
}
