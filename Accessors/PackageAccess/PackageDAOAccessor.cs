﻿using ClientOperationsWeb.Data.Domain;
using NHibernate;
using System.Collections.Generic;

namespace ClientOperationsWeb.Accessors.PackageAccess
{

    public interface PackageDAO
    {
        IList<Package> GetAllUserPackages(int id);

        IList<Package> SearchUserPackages(int id, string name);
    }

    public class PackageDAOAccessor : PackageDAO
    {
        private ISessionFactory factory;

        public PackageDAOAccessor(ISessionFactory factory)
        {
            this.factory = factory;
        }

        public IList<Package> GetAllUserPackages(int id)
        {
            ISession session = factory.OpenSession();
            ITransaction tx = null;
            IList<Package> list = null;

            try
            {
                tx = session.BeginTransaction();

                list = session
                    .CreateQuery("FROM Package WHERE suid = :id OR duid = :id")
                    .SetParameter("id", id)
                    .List<Package>();

                tx.Commit();
            }
            catch (HibernateException e)
            {
                if (tx != null)
                    tx.Rollback();
            }
            finally
            {
                session.Close();
            }

            return list;
        }

        public IList<Package> SearchUserPackages(int id, string name)
        {
            ISession session = factory.OpenSession();
            ITransaction tx = null;
            IList<Package> list = null;

            try
            {
                tx = session.BeginTransaction();

                list = session
                    .CreateQuery("FROM Package WHERE (suid = :id OR duid = :id) AND lower(name) = :name")
                    .SetParameter("id", id)
                    .SetParameter("name", name.ToLower())
                    .List<Package>();

                tx.Commit();
            }
            catch (HibernateException e)
            {
                if (tx != null)
                    tx.Rollback();
            }
            finally
            {
                session.Close();
            }

            return list;
        }
    }
}
