﻿using ClientOperationsWeb.Data.Domain;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientOperationsWeb.Accessors.RouteAccess
{
    public interface RouteDAO
    {
        IList<Route> GetPackageRoute(int id);
    }

    public class RouteDAOAccessor : RouteDAO
    {
        private ISessionFactory factory;

        public RouteDAOAccessor(ISessionFactory factory)
        {
            this.factory = factory;
        }

        public IList<Route> GetPackageRoute(int id)
        {
            ISession session = factory.OpenSession();
            ITransaction tx = null;
            IList<Route> list = null;

            try
            {
                tx = session.BeginTransaction();

                list = session
                    .CreateQuery("FROM Route WHERE pid = :id")
                    .SetParameter("id", id)
                    .List<Route>();

                tx.Commit();
            }
            catch (HibernateException e)
            {
                if (tx != null)
                    tx.Rollback();
            }
            finally
            {
                session.Close();
            }

            return list;
        }

    }
}
